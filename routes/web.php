<?php

use Illuminate\Auth\Middleware\Authenticate_autentificacion as Middleware;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//USUARIO NORMAL
Route::get('/auth/google', 'Auth\LoginController@redirectToGoogle');
Route::get('/callback', 'Auth\LoginController@handleGoogleCallback');


Route::prefix('/profesor')->group(function(){
    Auth::routes();
    Route::get('/home', 'HomeController@index')->middleware('guest')->middleware('profesor');


    Route::get('/crear_incidencia', 'HomeController@crear_incidencia')->middleware('profesor');
    Route::get('/editar_incidencia/{codigo}', 'HomeController@editar_incidencia')->middleware('profesor');
    Route::get('/eliminar_incidencia/{codigo}','HomeController@eliminar_incidencia')->middleware('profesor');
    Route::get('/ver_historial', 'HomeController@historial_incidencia')->middleware('profesor');

    Route::post('/crear_incidencia_nueva', 'HomeController@crear_incidencia_nueva')->middleware('profesor');
    Route::post('/guardar_incidencia_editada', 'HomeController@guardar_incidencia_editada')->middleware('profesor');
    
    Route::get('/cancelar','HomeController@cancelar')->middleware('profesor');

   // Route::get('/subir_archivo','HomeController@subir_archivo')->middleware('profesor');
});




//USUARIO ADMIN
Route::prefix('/admin')->group(function(){
    Auth::routes();
    Route::get('/home', 'AdminController@index')->middleware('admin');
    Route::get('/editar_incidencia/{codigo}', 'AdminController@editar_incidencia')->middleware('admin');
    Route::post('/guardar_incidencia_editada/{codigo}', 'AdminController@guardar_incidencia_editada')->middleware('admin');

    Route::get('/eliminar_incidencia/{codigo}','AdminController@eliminar_incidencia')->middleware('admin');

    Route::get('/cancelar','AdminController@cancelar')->middleware('admin');

   // Route::get('/subir_archivo','AdminController@subir_archivo')->middleware('admin');
});
