@extends('layouts.app2')
<style> 


.content {
        text-align: center;
    }

.title {
        font-size: 50px;
    }

.m-b-md {
        margin-bottom: 5px;
    }
#avatar{
    position: relative;
    float: right;
    top: -10px;
}



.botones input{
    position: absolute;
    left: 110%;
    height: 40px;
}

input{
    padding: 0px;
    margin: 0px;
}

#boton_historial{
    left: -25%;
}




*{
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
}
body{
    font-family: Helvetica;
    -webkit-font-smoothing: antialiased;
    background: rgba( 71, 147, 227, 1);
}
h2{
    text-align: center;
    font-size: 18px;
    text-transform: uppercase;
    letter-spacing: 1px;
    color: white;
    padding: 30px 0;
}

/* Table Styles */

.table-wrapper{
    margin: 10px 0px 0px;
    box-shadow: 0px 35px 50px rgba( 0, 0, 0, 0.2 );
}

.fl-table {
    border-radius: 5px;
    font-size: 12px;
    font-weight: normal;
    border: none;
    border-collapse: collapse;
    width: 100%;
    max-width: 100%;
    white-space: nowrap;

}

.fl-table td, .fl-table th {
    text-align: center;
    padding: 8px;
}

.fl-table td {
    border-right: 1px solid black;
    border-bottom: 1px solid black;
    font-size: 12px;
}

.fl-table thead th {
    color: #000;
    background: #95FF4F;
}


.fl-table thead th:nth-child(odd) {
    color: #000;
    background: #52B80E;
}

.fl-table tr:nth-child(even) {
    background: #F8F8F8;
}

/* Responsive */

@media (max-width: 767px) {
    .fl-table {
        display: block;
        width: 100%;
    }
    .table-wrapper:before{
        content: "Scroll horizontally >";
        display: block;
        text-align: right;
        font-size: 11px;
        color: white;
        padding: 0 0 10px;
    }
    .fl-table thead, .fl-table tbody, .fl-table thead th {
        display: block;
    }
    .fl-table thead th:last-child{
        border-bottom: none;
    }
    .fl-table thead {
        float: left;
    }
    .fl-table tbody {
        width: auto;
        position: relative;
        overflow-x: auto;
    }
    .fl-table td, .fl-table th {
        padding: 20px .625em .625em .625em;
        height: 60px;
        vertical-align: middle;
        box-sizing: border-box;
        overflow-x: hidden;
        overflow-y: auto;
        width: 120px;
        font-size: 13px;
        text-overflow: ellipsis;
    }
    .fl-table thead th {
        text-align: left;
        border-bottom: 1px solid #f7f7f9;
    }
    .fl-table tbody tr {
        display: table-cell;
    }
    .fl-table tbody tr:nth-child(odd) {
        background: none;
    }
    .fl-table tr:nth-child(even) {
        background: transparent;
    }
    .fl-table tr td:nth-child(odd) {
        background: #F8F8F8;
        border-right: 1px solid #E6E4E4;
    }
    .fl-table tr td:nth-child(even) {
        border-right: 1px solid #E6E4E4;
    }
    .fl-table tbody td {
        display: block;
        text-align: center;
    }
}

</style>


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
           
<!-- si esta logueado mostramos el nombre y avatar -->  
                  
                @if (Route::has('login'))
                @auth   
                <div class="card-header">{{ Auth::user()->name }}   <img id="avatar" style="float:right" src="{{ Auth::user()->avatar }}" width="49px" alt=""><strong><h5 style="position: relative;float:right;right:10px;;">HISTORIAL</h5></strong></div>
                        

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
        
                            <div class="botones">
        
                                <form action="/profesor/crear_incidencia" method="GET">
                                    <input type="submit" value="Crear incidencia">
                                </form>
            
<!-- Boton atras -->            
                            <form action="/profesor/cancelar" method="GET">
                                <input style="position:absolute;left:-200px" type="submit" value="Atras">
                            </form>
        
                            </div>
                            
                            
<div class="table-wrapper">
    <table class="fl-table">
        <thead>
            <tr>
                <center>
                <th>Codigo Incidencia</th>
                <th>Codigo Profesor</th>
                <th>Aula</th>
                <th>Edificio</th>
                <th>Codigo</th>
                <th>Descripcion</th>
                <th>Fecha</th>
                <th>Solucionado</th>
                </center>
           </tr>
        </thead>
        
        <script> 
            function eliminar(codigo){
                alert('La incidencia nº: '+codigo+' se ha eliminado correctamente');
            }
        </script>

        <tbody>
            @foreach ($datos as $dato)
                   @if ($dato['solucion'] == 'no')
                    <tr style="background-color:rgba(240, 0, 0, 0.77);color:black">
                        <td>{{$dato['codigo']}}</td>
                        <td>{{$dato['id_profesor']}}</td>
                        <td>{{$dato['clase']}}</td>
                        <td>{{$dato['edificio']}}</td>
                        <td>{{$dato['equipo']}}</td>
                        <td>{{$dato['descripcion']}}</td>
                        <td>{{$dato['created_at']}}</td>
                        <td>{{$dato['solucion']}}</td>
                        <td><a href="eliminar_incidencia/{{$dato->codigo}}"><img onclick="eliminar({{$dato['codigo']}})" height="30px" width="30px" src="http://pluspng.com/img-png/red-cross-png-red-cross-png-file-2000.png" alt=""></a></td>
                        <td><a href="editar_incidencia/{{$dato->codigo}}"><img height="30px" width="30px" src="http://freevector.co/wp-content/uploads/2012/01/61456-pencil-edit-button.png" alt=""></a></td>
                        <td><a href=""><img height="30px" width="30px" src="https://image.flaticon.com/icons/png/512/51/51862.png" alt=""></a></td>
                    </tr>
                   @else
                    <tr style="background-color:green">
                        <td>{{$dato['codigo']}}</td>
                        <td>{{$dato['id_profesor']}}</td>
                        <td>{{$dato['clase']}}</td>
                        <td>{{$dato['edificio']}}</td>
                        <td>{{$dato['equipo']}}</td>
                        <td>{{$dato['descripcion']}}</td>
                        <td>{{$dato['created_at']}}</td>
                        <td>{{$dato['solucion']}}</td>
                        <td><a href="eliminar_incidencia/{{$dato->codigo}}"><img onclick="eliminar({{$dato['codigo']}})" height="30px" width="30px" src="http://pluspng.com/img-png/red-cross-png-red-cross-png-file-2000.png" alt=""></a></td>
                        <td><a href="editar_incidencia/{{$dato->codigo}}"><img height="30px" width="30px" src="http://freevector.co/wp-content/uploads/2012/01/61456-pencil-edit-button.png" alt=""></a></td>
                        <td><a href=""><img height="30px" width="30px" src="https://image.flaticon.com/icons/png/512/51/51862.png" alt=""></a></td>
                    </tr>
                   @endif                         
                                            
           
            @endforeach

        <tbody>
    </table>

    <br>
     <div style="display:inline-table">{{ $datos->links() }}</div>
                   
                        

                @else
                    <div class="content">
                            <div class="title m-b-md">
                                No estas registrado
                            </div>

                            <br>

                            <div class="html,body m-b-md" style="font-size:20px">
                                El correo introducido no es valido o no requiere de los permisos necesarios
                            </div>
                            <div>
                                <a href="https://accounts.google.com/logout">
                                    Logout
                                </a>
                            </div>
                    </div>
                    @endauth
                @endif
                
                
            
        </div>
    </div>
</div>
@endsection
