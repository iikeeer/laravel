<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->

<!--    Style/fonts google    -->    
                <!-- Compiled and minified CSS -->
                
                <!-- Compiled and minified JavaScript -->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>


    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<style>

        #logo{  
            position: absolute;
        }

        .card-body{
            border-left: 1px solid skyblue;
            border-right: 1px solid skyblue;
            border-bottom: 1px solid skyblue;
            background-color: #F969FF;
            color: white;
        }
        .card-header{
            border-left: 1px solid skyblue;
            border-top: 1px solid skyblue;
            border-right: 1px solid skyblue;
            background-color: #AC15B3;
        }

        #avatar{
            position: relative;
            float: right;
            top: -12px; 
        }   
        .formularios{
            width: auto;
        }
        body{
            
            color: white;
        }

        input{
            background-color: #AC15B3;
            border: 3px solid #95FF4F;
            padding-left: 10px;
            padding-right: 10px;
            color: #fff;
        }
        select{
            background-color: #AC15B3;
            border: 3px solid #95FF4F;
            padding-left: 10px;
            padding-right: 10px;
            color: #fff;
        }
        
        .col-md-8{
            max-width: 100%;
        }
       


</style>




<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest

<!-- Barra de navegacion superior solo con login with google -->                        
                            <li class="nav-item">
                                <div class="col s12 m6 offset-m3 center-align">
                                    <a class="oauth-container btn darken-4 white black-text" href="/auth/google/" style="text-transform:none">
                                        <div class="left">
                                            <img width="20px" style="margin-top:3px; margin-right:8px;" alt="Google sign-in" 
                                                src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png" />
                                        </div>
                                        Login with Google
                                    </a>
                            </div>
                            </li>



                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <img src="{{ Auth::user()->avatar }}" alt="" width="50px">  <span class="caret"></span>
                                </a>

                                <a href=""><img style="position:absolute; right:-40%; top:20%;" src="http://cdn.onlinewebfonts.com/svg/img_489991.png" width="45px" height="45px" alt=""></a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                    <iframe id="logoutframe" src="https://accounts.google.com/logout" style="display: none"></iframe>
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                               
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
