<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mensaje Recibido</title>
</head>
<body>

    Se ha solucionado la incidencia del aula {{$mensaje['clase']}} con exito,
    el equipo {{$mensaje['equipo']}} del edificio {{$mensaje ['edificio'] }}, tenia problemas
    con Descripcion: {{$mensaje['descripcion']}}
    <br>
    Un saludo,
    <br>
    El Administrador de Plaiaundi ®

</body>
</html>