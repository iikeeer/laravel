@extends('layouts.app2')
<style> 


.content {
        text-align: center;
    }

.title {
        font-size: 50px;
    }

.m-b-md {
        margin-bottom: 5px;
    }
#avatar{
    position: relative;
    float: right;
    top: -10px;
}

#tabla{

}

</style>


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
           
<!-- si esta logueado mostramos el nombre y avatar -->       
                @if (Route::has('login'))
                    @auth
<!-- Si la base de datos esta vacia -->                   
                    @if($datos->isEmpty())
                    
                        <label>No hay ninguna incidencia, buen trabajo, sigue así :)</label>
<!-- Sino rellenamos la pagina -->
                    @else
                        @foreach ($datos as $dato)
                            <div class="card-header">Incidencia Nº {{$dato['codigo']}},  {{ Auth::user()->name }} <img id="avatar" src="{{ Auth::user()->avatar }}" width="45px" alt=""><strong><h5 style="position: relative;float:right;right:10px;">EDITAR INCIDENCIA</h5></strong></div>


                            <div class="card-body">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                

                                <form action="/admin/guardar_incidencia_editada/{{$dato['codigo']}}" method="POST">
                                    

                                    <label>Clase:
                                        <input type="text" name="clase" placeholder="Clase" value="{{$dato['clase']}}">
                                    </label>

                                    <label>Equipo:
                                        <input type="text" name="equipo" placeholder="Equipo" value="{{$dato['equipo']}}">
                                    </label>

                                    <label>Descripcion:
                                        <input type="text" name="descripcion" placeholder="Descripcion" value="{{$dato['descripcion']}}">
                                    </label>

                                    <label>Edificio:
                                        <select name="edificio" id="" >
                                            <option>{{$dato['edificio']}}</option>
                                            <option value="aiakoharria"> Aiako Harria</option>
                                            <option value="hondarribi"> Hondarribi</option>
                                            <option value="jaizkibel"> Jaizkibel</option>
                                        </select>
                                    </label>

                                    <br>
                                    <input style="position:relative;left:200px;top:50px" type="submit" value="Guardar">
                                    
                                </form>

                                <form action="/profesor/cancelar" method="GET">
                                    <input style="position:relative;left:400px" type="submit" value="Cancelar">
                                </form>
                            </div>
                            @endforeach
                    @endif

                @else
                    <div class="content">
                            <div class="title m-b-md">
                                No estas registrado
                            </div>

                            <br>

                            <div class="html,body m-b-md" style="font-size:20px">
                                El correo introducido no es valido o no requiere de los permisos necesarios
                            </div>
                    </div>
                    @endauth
                @endif
                
                
             
        </div>
    </div>
</div>
@endsection
