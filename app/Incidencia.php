<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Incidencia extends Model

    {
        use Notifiable;
        public function Incendia(){
            return $this->hasOne("App/User");
        }

        protected $fillable = [
            'codigo', 'id_profesor', 'clase', 'equipo', 'edificio', 'descripcion',
        ];
    }
