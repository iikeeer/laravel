<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\User;
use App\Incidencia;
use Auth;
use Expeciton;
use App\Rules\Equipo;
use Validator;
use DB;
use App\Mail\MensajeRecibido;
use Illuminate\Support\Facades\Mail;


class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('autentificacion');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
       if(Auth::check()){
    
        $incidencia = Incidencia::select('codigo','id_profesor','clase','edificio','equipo','descripcion','created_at','solucion')->paginate(5);

        return view('home_admin',['datos' => $incidencia]);
       }
       else{
           return view('home_admin');
       }

    }

    public function crear_incidencia(Request $request){
        return view('crear_incidencia');
    }

    public function crear_incidencia_nueva(Request $request){
        $datosValidados = Validator::make($request->all(),[
            'clase' => 'required|min:2|numeric',
            'equipo' => ['required','string', new Equipo],
            'descripcion' => 'required|max:50',
            'edificio' => 'required|in:aiakoharria,hondarribi,jaizkibel'
        ]);


        if($datosValidados->fails()){
            return back()
                ->withErrors($datosValidados)
                ->withInput();
        }

        else{

            $id = Auth::user()->id;
            $incidencia = new Incidencia;
            $incidencia->id_profesor = $id;
            $incidencia->clase = $request->clase;
            $incidencia->equipo = $request->equipo;
            $incidencia->edificio = $request->edificio;
            $incidencia->descripcion = $request->descripcion;
            $incidencia->save();
                
               
            
            return redirect('admin/home');
       
        }
    }


    public function eliminar_incidencia($codigo)
    {
        $eliminar = Incidencia::WHERE('codigo',$codigo)->delete();
        return redirect('admin/home');
    }



    public function editar_incidencia($codigo)
    {   
        if(Auth::check()){
            $incidencia = Incidencia::SELECT('codigo','id_profesor','clase','edificio','equipo','descripcion','created_at','solucion')->WHERE('codigo',$codigo)->get();
            return view('editar_incidencias', ['datos' => $incidencia]);
        }
    }

    public function guardar_incidencia_editada(Request $request, $codigo){
        $datosValidados = Validator::make($request->all(),[
            'clase' => 'required|min:2|numeric',
            'equipo' => ['required','string', new Equipo],
            'descripcion' => 'required|max:50',
            'edificio' => 'required|in:aiakoharria,hondarribi,jaizkibel'
        ]);

        if($datosValidados->fails()){
            return back()
                ->withErrors($datosValidados)
                ->withInput();
        }
        else{

            $incidencia = Incidencia::SELECT ('*')->where('codigo',$codigo);
            

            $incidencia->clase = $request->clase;
            $incidencia->equipo = $request->equipo;
            $incidencia->edificio = $request->edificio;
            $incidencia->descripcion = $request->descripcion;
            $incidencia->update($request->all());
//EMAIL
            $mensaje = $request->all();
            
            $receptor_correo = User::get();

            foreach ($receptor_correo as $receptor) {
                Mail::to($receptor->email)->send(new MensajeRecibido($mensaje));
            }
            
                
            return redirect('admin/home');
        }
    }


    public function historial_incidencia(Request $request){
        if(Auth::check()){
            $id = Auth::user()->id;
            $incidencias = Incidencia::SELECT('codigo','id_profesor','clase','edificio','equipo','descripcion','created_at','solucion')->WHERE('id_profesor',$id)->paginate(5);
            return view('historial_incidencia', ['datos'=>$incidencias]);
        }

        return view('historial_incidencia');
    }

    public function cancelar(Request $request){
        return redirect('admin/home');
    }

    /*
    public function save(Request $request)
{
 
       //obtenemos el campo file definido en el formulario
       $file = $request->file('file');
 
       //obtenemos el nombre del archivo
       $nombre = $file->getClientOriginalName();
 
       //indicamos que queremos guardar un nuevo archivo en el disco local
       \Storage::disk('local')->put($nombre,  \File::get($file));
 
       return "archivo guardado";
}

*/
}
